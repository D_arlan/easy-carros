import axios from 'axios'
import { getToken } from "./auth";
const api = axios.create({
    baseURL: "http://localhost:8181",
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
})

api.interceptors.request.use(async config => {
    const token = getToken();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});

export default api;