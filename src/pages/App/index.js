import React, { useState, useEffect } from 'react'
import api from '../../services/api'
import Modal from '../../Components/Modal/'
import BootBox from 'react-bootbox';


import { FiTrash2, FiPlus } from 'react-icons/fi'
import './style.css'
import logoImg from '../../assets/logo.svg'


export default function Login() {

    const [vehicles, setVehicles] = useState([])
    const [plate, setPlate] = useState('')
    const token = localStorage.getItem('@easy-Token')

    useEffect(
        
    () => {
        api.get('vehicle', {
            headers: {
                Authorization: token,
            }
        }).then(response => {
           
            setVehicles(response.data.data)
        })
    }, [token],
    )

    async function handleDeleteVehicle(id) {
        try {
            await api.delete(`vehicle/${id}`, {
                headers: {
                    Authorization: token,
                }
            });
            setVehicles(vehicles.filter(vehicle => vehicle.id !== id))
        } catch (err) {
            alert('Erro ao deletar caso tente novamente!')
        }
    }
    async function registerVehicle() {
        setShowConfirm(false)
            const data = {
                plate
            }
            try {
                const response = await api.post(`vehicle`, data,{
                    headers: {
                        Authorization: token,
                    }
                })
                setPlate('')
                const newVeicle = response.data.data
                setVehicles([...vehicles, newVeicle])

            } catch (err) {
                alert('Erro ao cadastrar tente novamente!')
            }
    }
    
  

    const numbers = vehicles;
    const listItems = numbers.map((vehicle) =>

        <li key={vehicle.id}>
            <span>{vehicle.plate.toUpperCase() }</span>
            <button onClick={() => handleDeleteVehicle(vehicle.id)} type="button">
            <FiTrash2 size={20} color="#444" />
        </button>
        </li>
 
        
    );

    const [showConfirm, setShowConfirm] = useState(false);
   
    async function confirmModal(e) {
        e.preventDefault();
        if(plate)
        setShowConfirm(true)
    }
    return (
        
        
        
        <div className="profile-container">
            {/* <Modal show={show} register={(res) => register(res)} title={`Adicionar veiculo?${plate}`}/> */}

            <BootBox 
                type={"confirm"}  
                message="Adicionar veiculo?"
                show={showConfirm}   
                onYesClick = {() =>registerVehicle()}          
                onNoClick = {() => setShowConfirm(false)}          
                onClose = {() => setShowConfirm(false)}
            />
            <header>
                    <img className="logo" src={logoImg} alt="Logo Easy Carros" />
                </header>
                

            {/* <Modal show={true}/> */}
            <section className="form">
                
                <form onSubmit={confirmModal}>
                <h4>Adicionar novo veículo</h4>
                <div className="col-md-6 col-12">
                   
                        <div className="row">
                        <input 
                            type="text"
                            placeholder="Placa"
                            value={plate}
                            className="col-md-8 col-8"
                            maxLength='7'
                            onChange={e => setPlate(e.target.value)}
                            style={{marginTop:16, marginRight:5, width:'92%'}}
                        />
                        <button style={{ width:80, lineHeight:0 }} className="button col-md-2 col-3" type="submit">
                            <FiPlus size={30} color="#FFF" />
                        </button>
                        </div>
                        
                    
                </div>
                </form>
                <h1>Placas Cadastradas</h1>
                <ul>
                    {listItems}
                </ul>
                
            </section>
        </div>
    );
}
