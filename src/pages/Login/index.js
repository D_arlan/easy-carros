import React, { useState } from 'react'
import api from '../../services/api'
import { login } from "../../services/auth";

import { useHistory } from 'react-router-dom'
import './style.css'
import logoImg from '../../assets/logo.svg'


export default function Logon() {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    
    const history = useHistory()
    
    async function handleLogin(e) {
        e.preventDefault();
        try {
            const response = await api.post('auth', { email, password })
            console.log(response.data);

            login(response.data.data.token);
            history.push('/app')
            
            
        }
        catch (err) {
            alert('Falha no login, tente novamente')
        }
    }

    return (
        <div className="logon-container">
            <section className="form">
                <img src={logoImg} alt="Logo Easy Carros" />
                <form onSubmit={handleLogin}>
                
                    <h1>Seja Bem-Vindo Easy Carros</h1>
                    <input
                        type="email"
                        placeholder="Email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <input
                    type="password"
                        placeholder="Senha"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                    />
                    <button className="button" type="submit">Entrar</button>

                    {/* <Link className="back-link" to="/register"><FiLogIn size={16} color="#E02041" />Não tenho cadastro</Link> */}
                </form>
            </section>
        </div>
    );
}
