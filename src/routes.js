import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import { isAuthenticated } from "./services/auth";

import Login from './pages/Login'
import App from './pages/App'
//import Register from './pages/Register'
//import Profile from './pages/Profile'
//import NewIncident from './pages/NewIncident'



const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );

  const Routes = () => (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/signup" component={() => <h1>SignUp</h1>} />
        <PrivateRoute path="/app" component={App} />
        <Route path="*" component={() => <h1>Page not found</h1>} />
      </Switch>
    </BrowserRouter>
  );
  
  export default Routes;

// export default function Routes() {
//     return (
//         <BrowserRouter>
//             <Switch>
//                 <Route path="/" exact component={Login} />
//                 {/* <Route path="/register" component={Register} />
//                 <Route path="/profile" component={Profile} />
//                 <Route path="/incident/new" component={NewIncident} /> */}
//             </Switch>
//         </BrowserRouter>
//     )
// }