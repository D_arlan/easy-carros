import React from "react";
import './style.css'
export default function Modal(props){
  
      if(!props.show){
          return null;
      }

      async function registerSim(e){
        e.preventDefault();
        props.register(true)

      }
    return (
      <div className="modal" id="modal">
        <div className="box">
          <div className="header">
            <h2>{props.title}</h2>
          </div>
        
        <div className="actions">
          <button style={{color:'#444', background:"#fff", border:"1px solid #03c" }}  >
            Não
          </button>
          <button onClick={() => registerSim} >
            Sim
          </button>
        </div>
        </div>
        
      </div>
    )
  }
